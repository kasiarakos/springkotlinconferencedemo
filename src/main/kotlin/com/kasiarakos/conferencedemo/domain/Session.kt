package com.kasiarakos.conferencedemo.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.*

@Entity(name = "sessions")
data class Session(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "session_id")
        val id: Int?,

        @Column(name = "session_name")
        val name: String,

        @Column(name = "session_description")
        val description: String,

        @Column(name = "session_length")
        val length: Int,

        @ManyToMany
        @JoinTable(
                name = "session_speakers",
                joinColumns = [JoinColumn(name = "session_id")],
                inverseJoinColumns = [JoinColumn(name = "speaker_id")]
        )
        @JsonIgnoreProperties("sessions")
        var speakers: List<Speaker>?
){
        override fun toString(): String {
                return "Session(id=${this.id}, name=${name}, description=${description}, length=${length}, speakers=[${speakers?.size}])"
        }
}