package com.kasiarakos.conferencedemo.domain

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.hibernate.annotations.Type
import javax.persistence.*

@Entity(name = "speakers")
data class Speaker(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "speaker_id")
        val id: Int?,

        @Column(name = "first_name")
        val firstName: String,

        @Column(name = "last_name")
        val lastName: String,

        @Column(name = "title")
        val title: String,

        @Column(name = "company")
        val company: String,

        @Column(name = "speaker_bio")
        val speakerBio: String,

        @ManyToMany(mappedBy = "speakers")
        @JsonIgnoreProperties("speakers")
        val sessions: List<Session>?,

        @Lob
        @Type(type = "org.hibernate.type.BinaryType")
        @Column(name = "speaker_photo")
        val photo: List<Byte>?

) {
    @PreRemove
    fun removeSpeakerFromSessions() {
        if (sessions != null) {
            for (session: Session in sessions) {
                session.speakers = session.speakers?.filter { it != this }
            }
        }
    }
}