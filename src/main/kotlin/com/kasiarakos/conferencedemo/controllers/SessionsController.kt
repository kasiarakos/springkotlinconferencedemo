package com.kasiarakos.conferencedemo.controllers

import com.kasiarakos.conferencedemo.domain.Session
import com.kasiarakos.conferencedemo.services.SessionsService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1/sessions")
class SessionsController constructor(val sessionsService: SessionsService) {

    @GetMapping
    fun list(): List<Session> = sessionsService.findAll()

    @GetMapping("/{id}")
    fun getById(@PathVariable id: Int) = sessionsService.findById(id)

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody session: Session) =  sessionsService.create(session)

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Int) = sessionsService.delete(id)

    @PutMapping("/{id}")
    fun update(@PathVariable id: Int, @RequestBody session: Session) = sessionsService.update(id, session)

}