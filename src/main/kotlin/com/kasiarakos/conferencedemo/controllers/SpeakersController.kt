package com.kasiarakos.conferencedemo.controllers

import com.kasiarakos.conferencedemo.domain.Speaker
import com.kasiarakos.conferencedemo.services.SpeakersService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1/speakers")
class SpeakersController(val speakersService: SpeakersService) {

    @GetMapping
    fun list(): List<Speaker> = speakersService.findAll()

    @GetMapping("/{id}")
    fun findById(@PathVariable id: Int) = speakersService.findById(id)

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    fun create(@RequestBody speaker: Speaker) = speakersService.create(speaker)

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable id: Int) = speakersService.deleteById(id)

    @PutMapping("/{id}")
    fun update(@PathVariable id: Int, @RequestBody speaker: Speaker) = speakersService.update(id, speaker)
}