package com.kasiarakos.conferencedemo.services

import com.kasiarakos.conferencedemo.domain.Speaker

interface SpeakersService {

    fun findAll() : List<Speaker>
    fun findById(id: Int) : Speaker?
    fun create(speaker: Speaker) : Speaker
    fun deleteById(id: Int)
    fun update(id: Int, speaker: Speaker) : Speaker
}