package com.kasiarakos.conferencedemo.services.impl

import com.kasiarakos.conferencedemo.domain.Speaker
import com.kasiarakos.conferencedemo.repositories.SpeakerRepository
import com.kasiarakos.conferencedemo.services.SpeakersService
import org.springframework.stereotype.Service

@Service
class SpeakersServiceImpl(val speakerRepository: SpeakerRepository) : SpeakersService {
    override fun update(id: Int, speaker: Speaker) : Speaker{
        val currentSpeaker = speakerRepository.findById(id).get()
        val updatedSpeaker = currentSpeaker.copy(firstName = speaker.firstName, lastName = speaker.lastName, title = speaker.title, company = speaker.company, speakerBio = speaker.speakerBio, sessions = speaker.sessions, photo = speaker.photo)
        speakerRepository.saveAndFlush(updatedSpeaker)
        return updatedSpeaker
    }

    override fun deleteById(id: Int) {
        speakerRepository.deleteById(id)
    }

    override fun create(speaker: Speaker): Speaker {
        return speakerRepository.saveAndFlush(speaker)
    }

    override fun findById(id: Int): Speaker? {
       return speakerRepository.findById(id).orElse(null)
    }

    override fun findAll(): List<Speaker> {
        return speakerRepository.findAll()
    }
}