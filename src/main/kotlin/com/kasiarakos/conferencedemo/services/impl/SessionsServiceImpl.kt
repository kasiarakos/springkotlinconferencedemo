package com.kasiarakos.conferencedemo.services.impl

import com.kasiarakos.conferencedemo.domain.Session
import com.kasiarakos.conferencedemo.repositories.SessionRepository
import com.kasiarakos.conferencedemo.services.SessionsService
import org.springframework.stereotype.Service

@Service
class SessionsServiceImpl(val sessionRepository: SessionRepository) : SessionsService {

    override fun update(id: Int, session: Session): Session {
        val currentSession = sessionRepository.findById(id).get()
        val updatedSession = currentSession.copy(name = session.name, description = session.description, length = session.length, speakers = session.speakers)
        sessionRepository.saveAndFlush(updatedSession)
        return updatedSession
    }

    override fun delete(id: Int) {
        sessionRepository.deleteById(id)
    }

    override fun create(session: Session): Session {
        return sessionRepository.saveAndFlush(session)
    }

    override fun findById(id: Int): Session? {
        return sessionRepository.findById(id).orElse(null)
    }

    override fun findAll(): List<Session> {
        return sessionRepository.findAll()
    }
}
