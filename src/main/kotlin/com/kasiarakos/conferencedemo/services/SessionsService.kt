package com.kasiarakos.conferencedemo.services

import com.kasiarakos.conferencedemo.domain.Session

interface SessionsService {

    fun findAll() : List<Session>
    fun findById(id: Int) : Session?
    fun create(session: Session) : Session
    fun delete(id: Int)
    fun update(id: Int, session: Session) : Session
}