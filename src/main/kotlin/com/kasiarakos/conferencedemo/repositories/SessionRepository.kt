package com.kasiarakos.conferencedemo.repositories

import com.kasiarakos.conferencedemo.domain.Session
import org.springframework.data.jpa.repository.JpaRepository

interface SessionRepository : JpaRepository<Session, Int?>