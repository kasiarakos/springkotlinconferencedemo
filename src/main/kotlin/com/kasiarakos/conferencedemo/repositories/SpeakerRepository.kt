package com.kasiarakos.conferencedemo.repositories

import com.kasiarakos.conferencedemo.domain.Speaker
import org.springframework.data.jpa.repository.JpaRepository

interface SpeakerRepository : JpaRepository<Speaker, Int?>